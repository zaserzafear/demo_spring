package com.zaserzafear.demo_spring.entity.converter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

public class StatusEnum {
    @RequiredArgsConstructor
    public enum CommonStatus {
        APPROVED("A"),
        NOT_APPROVED("N"),
        PENDING("P");
        @Getter
        private final String code;

        public static StatusEnum.CommonStatus codeToStatus(String code) {
            return Stream.of(StatusEnum.CommonStatus.values()).parallel()
                    .filter(CommonStatus -> CommonStatus.getCode().equalsIgnoreCase(code))
                    .findAny().orElseThrow(() -> new IllegalArgumentException("The code : " + code + " is illegal argument."));
        }
    }
}
