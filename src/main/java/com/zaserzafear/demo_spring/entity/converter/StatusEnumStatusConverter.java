package com.zaserzafear.demo_spring.entity.converter;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

@Component
@Converter(autoApply = true)
public class StatusEnumStatusConverter implements AttributeConverter<StatusEnum.CommonStatus, String> {
    @Override
    public String convertToDatabaseColumn(StatusEnum.CommonStatus status) {
        return Objects.isNull(status) ? null : status.getCode();
    }

    @Override
    public StatusEnum.CommonStatus convertToEntityAttribute(String code) {
        return Objects.isNull(code) ? null : StatusEnum.CommonStatus.codeToStatus(code);
    }
}
