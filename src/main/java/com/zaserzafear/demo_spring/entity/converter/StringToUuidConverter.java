package com.zaserzafear.demo_spring.entity.converter;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class StringToUuidConverter {
    public UUID uuid(String s) {
        return UUID.fromString(s.replaceFirst(
                "(\\p{XDigit}{8})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}{4})(\\p{XDigit}+)", "$1-$2-$3-$4-$5"
        ));
    }
}
