package com.zaserzafear.demo_spring.entity.converter;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

@Component
@Converter(autoApply = true)
public class InOutEnumConverter implements AttributeConverter<InOutEnum.Action, String> {
    @Override
    public String convertToDatabaseColumn(InOutEnum.Action status) {
        return Objects.isNull(status) ? null : status.getCode();
    }

    @Override
    public InOutEnum.Action convertToEntityAttribute(String code) {
        return Objects.isNull(code) ? null : InOutEnum.Action.codeToStatus(code);
    }
}
