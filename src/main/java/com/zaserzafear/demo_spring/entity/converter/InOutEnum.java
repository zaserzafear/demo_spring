package com.zaserzafear.demo_spring.entity.converter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

public class InOutEnum {

    @RequiredArgsConstructor
    public enum Action {
        IN("I"),
        OUT("O");
        @Getter
        private final String code;

        public static InOutEnum.Action codeToStatus(String code) {
            return Stream.of(InOutEnum.Action.values()).parallel()
                    .filter(Action -> Action.getCode().equalsIgnoreCase(code))
                    .findAny().orElseThrow(() -> new IllegalArgumentException("The code : " + code + " is illegal argument."));
        }
    }
}
