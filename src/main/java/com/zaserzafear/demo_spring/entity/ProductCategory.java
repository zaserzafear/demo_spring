package com.zaserzafear.demo_spring.entity;

import com.zaserzafear.demo_spring.entity.common.CommonEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "product_categories")
@Table(indexes = {
        @Index(name = "product_categories_code_name_unique", columnList = "code, name", unique = true)
})
public class ProductCategory extends CommonEntity {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String code;
    private String name;
}
