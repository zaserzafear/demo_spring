package com.zaserzafear.demo_spring.entity;

import com.zaserzafear.demo_spring.entity.common.CommonEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "accounts")
@DynamicInsert
@DynamicUpdate
@Table(indexes = {
        @Index(name = "accounts_fname_lname", columnList = "first_name, last_name")
})
public class Account extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String first_name;
    private String last_name;
    private Double coin;
}
