package com.zaserzafear.demo_spring.entity;

import com.zaserzafear.demo_spring.entity.common.CommonEntity;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Entity(name = "products")
@Table(indexes = {
        @Index(name = "products_code_unique", columnList = "code", unique = true),
        @Index(name = "products_status", columnList = "status")
})
@DynamicInsert
@DynamicUpdate
public class Product extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @NotNull
    private UUID category;
    private String code;
    private String name;
    private Integer quantity;
    private Double price;

    @Column(length = 1, nullable = false)
    private StatusEnum.CommonStatus status;
}
