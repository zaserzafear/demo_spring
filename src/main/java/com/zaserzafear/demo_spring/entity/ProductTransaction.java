package com.zaserzafear.demo_spring.entity;

import com.zaserzafear.demo_spring.entity.converter.InOutEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@Immutable
@Entity(name = "product_transactions")
@Table(indexes = {
        @Index(name = "product_code", columnList = "product")
})
public class ProductTransaction {

    @Column(length = 1, nullable = false)
    InOutEnum.Action action;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String product;
    private Integer quantity;
    private Double price;

    // for CommonRepository (findAll)
    private Boolean isDelete = Boolean.FALSE;

    // custom Constructor
    public ProductTransaction(String productCode, InOutEnum.Action action, Integer productQuantity, Double productPrice) {
        this.product = productCode;
        this.action = action;
        this.quantity = productQuantity;
        this.price = productPrice;
    }
}
