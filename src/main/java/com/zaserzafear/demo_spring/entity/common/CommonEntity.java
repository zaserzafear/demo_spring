package com.zaserzafear.demo_spring.entity.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.time.OffsetDateTime;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(value = CommonListeners.class)
public abstract class CommonEntity {

    private OffsetDateTime createdDate;
    private OffsetDateTime updatedDate;
    //    private UUID updatedBy;
    private Boolean isDeleted = Boolean.FALSE;
    private OffsetDateTime deletedDate;
    //    private UUID deletedBy;
    @Version
    private int version;
}
