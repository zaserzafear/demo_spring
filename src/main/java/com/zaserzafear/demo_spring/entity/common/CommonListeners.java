package com.zaserzafear.demo_spring.entity.common;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.OffsetDateTime;

public class CommonListeners<T extends CommonEntity> {

    @PrePersist
    private void prePersist(T e) {
        e.setCreatedDate(OffsetDateTime.now());
    }

    @PreUpdate
    private void preUpdate(T e) {
        e.setUpdatedDate(OffsetDateTime.now());
    }
}
