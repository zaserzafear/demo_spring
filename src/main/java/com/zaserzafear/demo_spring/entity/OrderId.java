package com.zaserzafear.demo_spring.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
@Data
public class OrderId implements Serializable {
    private UUID id;
    private UUID product;
}
