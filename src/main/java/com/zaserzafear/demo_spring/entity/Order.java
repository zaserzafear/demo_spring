package com.zaserzafear.demo_spring.entity;

import com.zaserzafear.demo_spring.entity.common.CommonEntity;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "orders")
@NoArgsConstructor
@Table(indexes = {
        @Index(name = "orders_status", columnList = "status"),
})
public class Order extends CommonEntity {

    @Column(length = 1, nullable = false)
    StatusEnum.CommonStatus status;
    @EmbeddedId
    @AttributeOverrides(value = {
            @AttributeOverride(name = "id", column = @Column(name = "o_id")),
            @AttributeOverride(name = "product", column = @Column(name = "p_id"))
    })
    private OrderId orderId;
    private UUID account;
    private Integer quantity;
    private Double price;
    private OffsetDateTime orderDate;
    // for create OrderId
    private transient UUID id;
    private transient UUID product;

    // custom constructor
    public Order(UUID id, UUID product, OffsetDateTime orderDate, UUID account, Integer quantity, Double price, StatusEnum.CommonStatus status) {
        this.id = id;
        this.product = product;
        this.orderDate = orderDate;
        this.account = account;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
    }

    @PrePersist
    private void createOrderId() {
        OrderId orderId = new OrderId();
        orderId.setId(this.id);
        orderId.setProduct(product);
        this.orderId = orderId;
    }
}
