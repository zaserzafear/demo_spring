package com.zaserzafear.demo_spring.config;

import com.zaserzafear.demo_spring.config.property.AsyncProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Log4j2
@Configuration
@RequiredArgsConstructor
public class AsyncConf implements AsyncConfigurer {
    private final AsyncProperties props;

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(props.getCorePoolSize());
        executor.setQueueCapacity(props.getQueueCapacity());
        executor.setMaxPoolSize(props.getMaxPoolSize());
        executor.setThreadNamePrefix(props.getThreadNamePrefix());
        executor.setKeepAliveSeconds(props.getKeepAliveSeconds());
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) -> {
            log.error("AsyncMethod::{}({})", method.getName(), params);
            log.error("Exception::{}", ex.getMessage());
            // save exception information to queue or database for retry
        };
    }
}
