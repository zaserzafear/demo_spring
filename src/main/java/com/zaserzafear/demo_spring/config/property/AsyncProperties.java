package com.zaserzafear.demo_spring.config.property;

import com.zaserzafear.demo_spring.config.YamlFactoryConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Component
@Validated
@PropertySource(value = "classpath:async.yml", factory = YamlFactoryConfig.class)
@ConfigurationProperties("custom.async")
public class AsyncProperties {
    @NotNull
    @Min(1)
    private Integer corePoolSize;
    @NotNull
    @Min(1)
    private Integer queueCapacity;
    @NotNull
    @Min(1)
    private Integer maxPoolSize;
    @NotNull
    @Min(1)
    @Max(60)
    private Integer keepAliveSeconds;
    private String threadNamePrefix;
}
