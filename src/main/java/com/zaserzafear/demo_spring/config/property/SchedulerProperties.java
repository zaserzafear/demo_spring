package com.zaserzafear.demo_spring.config.property;

import com.zaserzafear.demo_spring.config.YamlFactoryConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Component
@Validated
@PropertySource(value = "classpath:scheduler.yml", factory = YamlFactoryConfig.class)
@ConfigurationProperties("custom.scheduler")
public class SchedulerProperties {
    @NotNull
    @Min(1)
    private Integer corePoolSize;
}
