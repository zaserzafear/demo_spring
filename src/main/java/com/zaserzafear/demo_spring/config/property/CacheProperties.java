package com.zaserzafear.demo_spring.config.property;

import com.zaserzafear.demo_spring.config.YamlFactoryConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Component
@Validated
@PropertySource(value = "classpath:cache.yml", factory = YamlFactoryConfig.class)
@ConfigurationProperties("custom.cache")
public class CacheProperties {
    @NotNull
    @Min(5)
    private Integer productsSize;
    @NotNull
    @Min(1)
    @Max(10000)
    private Integer productSize;
    @NotNull
    @Min(5)
    private Integer ordersSize;
    @NotNull
    @Min(5)
    private Integer ordersRecycleBinSize;
    @NotNull
    @Min(1)
    @Max(20000)
    private Integer orderSize;
    @NotNull
    @Min(1)
    private Integer productTransactionsSize;
    @NotNull
    @Min(1)
    private Integer productCategorySize;
}
