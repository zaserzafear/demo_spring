package com.zaserzafear.demo_spring.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.zaserzafear.demo_spring.config.property.CacheProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.util.Arrays;

@Log4j2
@EnableCaching
@Configuration
@RequiredArgsConstructor
public class CacheConf {
    private final CacheProperties props;

    @Bean
    public SimpleCacheManager buildSimpleCacheManager() {
        CaffeineCache productsCache = buildCaffeineCache(CacheName.PRODUCTS, props.getProductsSize());
        CaffeineCache productsRecyclebinCache = buildCaffeineCache(CacheName.PRODUCTS_RECYCLEBIN, props.getProductsSize());
        CaffeineCache productIdCache = buildCaffeineCache(CacheName.PRODUCT_ID, props.getProductSize());
        CaffeineCache productCodeCache = buildCaffeineCache(CacheName.PRODUCT_CODE, props.getProductSize());

        CaffeineCache ordersCache = buildCaffeineCache(CacheName.ORDERS, props.getOrdersSize());
        CaffeineCache ordersRecycleBinCache = buildCaffeineCache(CacheName.ORDERS_RECYCLEBIN, props.getOrdersRecycleBinSize());
        CaffeineCache orderCache = buildCaffeineCache(CacheName.ORDER, props.getOrderSize());

        CaffeineCache productTransactionsCache = buildCaffeineCache(CacheName.PRODUCT_TRANSACTIONS, props.getProductTransactionsSize());

        CaffeineCache productCategory = buildCaffeineCache(CacheName.PRODUCT_CATEGORY, props.getProductCategorySize());

        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        simpleCacheManager.setCaches(Arrays.asList(productsCache, productsRecyclebinCache, productIdCache, productCodeCache, ordersCache, ordersRecycleBinCache, orderCache, productTransactionsCache, productCategory));

        simpleCacheManager.initializeCaches();
        return simpleCacheManager;
    }

    private CaffeineCache buildCaffeineCache(String name, long maxSize) {
        log.info(() -> "Build CaffeineCache[" + name + "] , maximumSize[" + maxSize + "]");
        return new CaffeineCache(name, Caffeine.newBuilder()
                .softValues()
                .maximumSize(maxSize)
                .expireAfterAccess(Duration.ofHours(1))
                .expireAfterWrite(Duration.ofHours(12))
                .build());
    }

    @Scheduled(cron = "0 0 0 * * *")
    @Caching(evict = {
            @CacheEvict(cacheNames = CacheName.PRODUCTS, allEntries = true),
            @CacheEvict(cacheNames = CacheName.PRODUCTS_RECYCLEBIN, allEntries = true),
            @CacheEvict(cacheNames = CacheName.PRODUCT_ID, allEntries = true),
            @CacheEvict(cacheNames = CacheName.PRODUCT_CODE, allEntries = true),

            @CacheEvict(cacheNames = CacheName.ORDERS, allEntries = true),
            @CacheEvict(cacheNames = CacheName.ORDERS_RECYCLEBIN, allEntries = true),
            @CacheEvict(cacheNames = CacheName.ORDER, allEntries = true),

            @CacheEvict(cacheNames = CacheName.PRODUCT_TRANSACTIONS, allEntries = true),

            @CacheEvict(cacheNames = CacheName.PRODUCT_CATEGORY, allEntries = true)
    })
    public void evictAll() {
    }

    public static class CacheName {
        public static final String PRODUCTS = "PRODUCTS";
        public static final String PRODUCTS_RECYCLEBIN = "PRODUCTS_RECYCLEBIN";
        public static final String PRODUCT_ID = "PRODUCT_ID";
        public static final String PRODUCT_CODE = "PRODUCT_CODE";

        public static final String ORDERS = "ORDERS";
        public static final String ORDERS_RECYCLEBIN = "ORDERS_RECYCLEBIN";
        public static final String ORDER = "ORDER";

        public static final String PRODUCT_TRANSACTIONS = "PRODUCT_TRANSACTIONS";

        public static final String PRODUCT_CATEGORY = "PRODUCT_CATEGORY";
    }
}
