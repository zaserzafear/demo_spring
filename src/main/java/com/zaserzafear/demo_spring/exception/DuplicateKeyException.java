package com.zaserzafear.demo_spring.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class DuplicateKeyException extends CommonException {
    private final HttpStatus status = HttpStatus.BAD_REQUEST;
    private final String code = "400";

    public DuplicateKeyException(String message) {
        super(message);
    }
}
