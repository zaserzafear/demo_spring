package com.zaserzafear.demo_spring.controller;

import com.zaserzafear.demo_spring.service.ProductTransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Log4j2
@RestController
@RequiredArgsConstructor
public class ProductTransactionController extends CommonController {

    private final ProductTransactionService productTransactionService;

    //  /product_transaction
    @GetMapping("/product_transaction")
    public ResponseEntity<List<Object[]>> product_transactions() {
        return ResponseEntity.ok(productTransactionService.findAll());
    }
}
