package com.zaserzafear.demo_spring.controller;

import com.zaserzafear.demo_spring.dto.order.OrderDto;
import com.zaserzafear.demo_spring.dto.order.OrderSummary;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import com.zaserzafear.demo_spring.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequiredArgsConstructor
public class OrderController extends CommonController {

    private final OrderService orderService;

    // insert
    @PostMapping("/orders")
    public ResponseEntity<OrderSummary> insertOrder(@RequestBody List<OrderDto> order) {
        return ResponseEntity.ok(orderService.save(order));
    }

    //  /orders
    //  /orders?status=PENDING
    @GetMapping("/orders")
    public ResponseEntity<List<OrderSummary>> findAll(@RequestParam(required = false) StatusEnum.CommonStatus status) {
        return ResponseEntity.ok(orderService.findAll(status));
    }

    //  /orders/973abff1-3eb9-46d8-aa9f-91866b277e2b
    @GetMapping("/orders/{o_id}")
    public ResponseEntity<List<OrderDto>> findById(@PathVariable UUID o_id) {
        return ResponseEntity.ok(orderService.findById(o_id));
    }

    // /orders?account/id=2b931c4e-81d6-40de-a257-fd18c31e9f13
    // /orders?account/id=2b931c4e-81d6-40de-a257-fd18c31e9f13?status=PENDING
    @GetMapping("/orders/account/{account_id}")
    public ResponseEntity<List<OrderSummary>> ordersByAccount(@PathVariable UUID account_id, StatusEnum.CommonStatus status) {
        return ResponseEntity.ok(orderService.findByAccount(account_id, status));
    }

    // update status
    // /orders/973abff1-3eb9-46d8-aa9f-91866b277e2b?status=APPROVED
    @PatchMapping("/orders/{o_id}")
    public ResponseEntity<List<OrderDto>> patchStatus(@PathVariable UUID o_id, @RequestParam StatusEnum.CommonStatus status) {
        return ResponseEntity.ok(orderService.patchStatus(o_id, status));
    }

    // delete
    // /orders/973abff1-3eb9-46d8-aa9f-91866b277e2b
    @DeleteMapping("/orders/{o_id}")
    public ResponseEntity<List<OrderDto>> delete(@PathVariable UUID o_id) {
        return ResponseEntity.ok(orderService.deleted(o_id));
    }

    //  /orders/recyclebin
    //  /orders/recyclebin?status=PENDING
//    @GetMapping("/orders/recyclebin")
//    public ResponseEntity<List<Object[]>> ordersRecycleBin(@RequestParam(required = false) String status) {
//        return ResponseEntity.ok(orderService.findAllRecycleBin(status));
//    }
}
