package com.zaserzafear.demo_spring.controller;

import com.zaserzafear.demo_spring.dto.product.ProductDto;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import com.zaserzafear.demo_spring.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequiredArgsConstructor
public class ProductController extends CommonController {

    private final ProductService productService;

    // insert
    @PostMapping("/products")
    public ResponseEntity<ProductDto> insertProduct(@RequestBody ProductDto productDto) {
        return ResponseEntity.ok(productService.save(productDto));
    }

    //  /products
    //  /products?status=PENDING
    @GetMapping("/products")
    public ResponseEntity<List<ProductDto>> findAll(@RequestParam(required = false) StatusEnum.CommonStatus status) {
        return ResponseEntity.ok(productService.findAll(status));
    }

    //  /products/category/9f553180-8369-4ac0-ac7b-7ab63090ceb4
    @GetMapping("/products/category/{product_category}")
    public ResponseEntity<List<ProductDto>> findAllByCategory(@PathVariable UUID product_category) {
        return ResponseEntity.ok(productService.findAllByCategory(product_category));
    }

    //  /products/id/9f553180-8369-4ac0-ac7b-7ab63090ceb4
    @GetMapping("/products/id/{product_id}")
    public ResponseEntity<ProductDto> findById(@PathVariable UUID product_id) {
        return ResponseEntity.ok(productService.findById(product_id));
    }

    //  /products/P0001
    @GetMapping("/products/code/{product_code}")
    public ResponseEntity<ProductDto> findByCode(@PathVariable String product_code) {
        return ResponseEntity.ok(productService.findByCode(product_code));
    }

    // update all field
    @PutMapping("/products")
    public ResponseEntity<ProductDto> replace(@RequestBody ProductDto productDto) {
        return ResponseEntity.ok(productService.replace(productDto));
    }

    // delete
    @DeleteMapping("/products/{product_id}")
    public ResponseEntity<ProductDto> delete(@PathVariable UUID product_id) {
        return ResponseEntity.ok(productService.delete(product_id));
    }

    //  /products/recyclebin
    //  /products/recyclebin?status=PENDING
    @GetMapping("/products/recyclebin")
    public ResponseEntity<List<ProductDto>> findAllRecycleBin(@RequestParam(required = false) StatusEnum.CommonStatus status) {
        return ResponseEntity.ok(productService.findAllRecycleBin(status));
    }

    //  /products/recyclebin/category/9f553180-8369-4ac0-ac7b-7ab63090ceb4
    @GetMapping("/products/recyclebin/category")
    public ResponseEntity<List<ProductDto>> findAllByCategoryRecycleBin(@PathVariable UUID product_category) {
        return ResponseEntity.ok(productService.findAllByCategoryRecycleBin(product_category));
    }
}
