package com.zaserzafear.demo_spring;

import com.zaserzafear.demo_spring.entity.ProductCategory;
import com.zaserzafear.demo_spring.repository.OrderRepository;
import com.zaserzafear.demo_spring.repository.ProductCategoryRepository;
import com.zaserzafear.demo_spring.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.UUID;
import java.util.stream.IntStream;

@Log4j2
@RequiredArgsConstructor
@SpringBootApplication
public class DemoSpringApplication implements CommandLineRunner {

    private final ProductCategoryRepository productCategoryRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;

    public static void main(String[] args) {
        System.err.close();
        SpringApplication.run(DemoSpringApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        insertData();
    }

    public void insertData() {
//        info("insert productCategory");
        ProductCategory productCategory1 = new ProductCategory();
        productCategory1.setId(UUID.fromString("9f553180-8369-4ac0-ac7b-7ab63090ceb4"));
        productCategory1.setCode("T0001");
        productCategory1.setName("เครื่องเขียน");
        productCategory1 = productCategoryRepository.save(productCategory1);

        ProductCategory productCategory2 = new ProductCategory();
        productCategory2.setId(UUID.fromString("f36c5e74-9e0e-4b48-b81d-b1cdb4fe2a81"));
        productCategory2.setCode("T0002");
        productCategory2.setName("เครื่องดื่ม");
        productCategory2 = productCategoryRepository.save(productCategory2);
//
//        List<Product> products = new ArrayList<>();
//        Integer i = 0;
//        while (i < 1000) {
//            products.add(new Product(null, productCategory1.getId(), "P" + i.toString(), "PRODUCT" + i.toString(), 100, 5d, APPROVED));
//            i++;
//        }
//        productRepository.saveAll(products);
//
//        List<Order> orders = new ArrayList<>();
//        Integer j = 0;
//        UUID o_id = UUID.fromString("c0cbc87b-18ac-4281-aa20-ce2ce3f05e61");
//        LocalDateTime o_date = LocalDateTime.now();
//
//        StopWatch stopWatch = new StopWatch();
//        stopWatch.start();
//        while (j < 1000) {
//            orders.add(new Order(o_id, "1st Account", "P" + j.toString(), 100, 5d, o_date, APPROVED));
//            j++;
//        }
//        orderRepository.saveAll(orders);
//        stopWatch.stop();
//        info("Elapsed Time : " + stopWatch.getTotalTimeSeconds() + " Sec");
//
//        info("finish");
    }

    private void info(String message) {
        IntStream.range(0, 3).forEach(i -> System.out.println());
        log.info(() -> message);
    }
}
