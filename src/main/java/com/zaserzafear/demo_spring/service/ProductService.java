package com.zaserzafear.demo_spring.service;

import com.zaserzafear.demo_spring.dto.product.ProductDto;
import com.zaserzafear.demo_spring.dto.product.ProductMapper;
import com.zaserzafear.demo_spring.entity.Product;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import com.zaserzafear.demo_spring.exception.DuplicateKeyException;
import com.zaserzafear.demo_spring.exception.NotFoundException;
import com.zaserzafear.demo_spring.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.zaserzafear.demo_spring.config.CacheConf.CacheName.*;

@Log4j2
@RequiredArgsConstructor
@Service
@CacheConfig(cacheNames = PRODUCTS)
public class ProductService {

    private static final String connectMsg = "Connect Database";

    private final ProductRepository productRepository;
    private final ProductMapper mapper;
    @Autowired
    private ProductService service; // Autowired for calling cache from another method of the same bean

    @CacheEvict(allEntries = true)
    public ProductDto save(ProductDto dto) {
        log.info("save");
        String code = dto.getCode();
        Optional<ProductDto> product = Optional.ofNullable(service.productByCode(code));
        if (product.isPresent())
            throw new DuplicateKeyException("Product Code : " + code + " Has Already Been Used for + " + product.get().getName());
        return mapper.map(productRepository.saveAndFlush(mapper.map(dto)));
    }

    public List<ProductDto> findAll(StatusEnum.CommonStatus status) {
        log.info("findAll");
        return service.dtoListFindAll(status);
    }

    public List<ProductDto> findAllByCategory(UUID category) {
        log.info("findAllByCategory");
        return service.dtoListFindByCategory(category);
    }

    public ProductDto findById(UUID id) {
        log.info("findById");
        Optional<ProductDto> product = Optional.ofNullable(service.productById(id));
        if (product.isEmpty()) throw new NotFoundException("Product Id : " + id + " Is Not Found In Database");
        return product.get();
    }

    public ProductDto findByCode(String code) {
        log.info("findByCode");
        Optional<ProductDto> product = Optional.ofNullable(service.productByCode(code));
        if (product.isEmpty()) throw new NotFoundException("Product Code : " + code + " Is Not Found In Database");
        return product.get();
    }

    public ProductDto delete(UUID id) {    // Soft Delete
        log.info("delete");
        Optional<ProductDto> product = Optional.ofNullable(service.productById(id));
        if (product.isEmpty()) throw new NotFoundException("Product id : " + id + " Not Found In Database");
        Product entity = mapper.map(product.get());
        entity.setIsDeleted(Boolean.TRUE);
        entity.setDeletedDate(OffsetDateTime.now());
        return service.replace(mapper.map(entity));
    }

    public List<ProductDto> findAllRecycleBin(StatusEnum.CommonStatus status) {
        log.info("findAllRecycleBin");
        return service.dtoListFindAllRecyclebin(status);
    }

    public List<ProductDto> findAllByCategoryRecycleBin(UUID category) {
        log.info("findByCategoryRecycleBin");
        return service.dtoListFindByCategoryRecyclebin(category);
    }

    @Cacheable(cacheNames = PRODUCT_ID, unless = "#result==null")
    public ProductDto productById(UUID id) {
        log.info(connectMsg);
        return mapper.map(productRepository.findId(id));
    }

    @Cacheable(cacheNames = PRODUCT_CODE, unless = "#result==null")
    public ProductDto productByCode(String code) {
        log.info(connectMsg);
        return mapper.map(productRepository.findByCode(code));
    }

    @Cacheable(unless = "#result.isEmpty()")
    public List<ProductDto> dtoListFindAll(StatusEnum.CommonStatus status) {
        log.info(connectMsg);
        return (status == null) ? mapper.map(productRepository.findAllByIsDeleted(Boolean.FALSE)) : mapper.map(productRepository.findAllByStatusAndIsDeleted(status, Boolean.FALSE));
    }

    @Cacheable(cacheNames = PRODUCTS_RECYCLEBIN, unless = "#result.isEmpty()")
    public List<ProductDto> dtoListFindAllRecyclebin(StatusEnum.CommonStatus status) {
        log.info(connectMsg);
        return (status == null) ? mapper.map(productRepository.findAllByIsDeleted(Boolean.TRUE)) : mapper.map(productRepository.findAllByStatusAndIsDeleted(status, Boolean.TRUE));
    }

    @Cacheable(unless = "#result.isEmpty()")
    public List<ProductDto> dtoListFindByCategory(UUID category) {
        log.info(connectMsg);
        return mapper.map(productRepository.findAllByCategoryAndIsDeleted(category, Boolean.FALSE));
    }

    @Cacheable(cacheNames = PRODUCTS_RECYCLEBIN, unless = "#result.isEmpty()")
    public List<ProductDto> dtoListFindByCategoryRecyclebin(UUID category) {
        log.info(connectMsg);
        return mapper.map(productRepository.findAllByCategoryAndIsDeleted(category, Boolean.TRUE));
    }

    @Caching(evict = {@CacheEvict(allEntries = true), @CacheEvict(cacheNames = PRODUCTS_RECYCLEBIN, allEntries = true), @CacheEvict(cacheNames = PRODUCT_CODE, allEntries = true)}, put = {@CachePut(cacheNames = PRODUCT_ID, key = "#dto.id")})
    public ProductDto replace(ProductDto dto) {
        log.info("replace");
        UUID id = dto.getId();
        Optional<ProductDto> product = Optional.ofNullable(service.productById(id));
        if (product.isEmpty()) throw new NotFoundException("Product id : " + id + " Not Found In Database");
        String code = dto.getCode();
        product = Optional.ofNullable(service.productByCode(code));
        if (product.isPresent() && !product.get().getId().equals(id))
            throw new DuplicateKeyException("Product Code : " + code + " Has Already Been Used for + " + product.get().getName());
        log.info(connectMsg);
        return mapper.map(productRepository.saveAndFlush(mapper.map(dto)));
    }
}
