package com.zaserzafear.demo_spring.service;

import com.zaserzafear.demo_spring.dto.order.OrderDto;
import com.zaserzafear.demo_spring.dto.order.OrderMapper;
import com.zaserzafear.demo_spring.dto.order.OrderSummary;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import com.zaserzafear.demo_spring.entity.converter.StatusEnumStatusConverter;
import com.zaserzafear.demo_spring.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static com.zaserzafear.demo_spring.config.CacheConf.CacheName.*;

@Log4j2
@RequiredArgsConstructor
@Service
@CacheConfig(cacheNames = ORDERS)
public class OrderService {

    private static final String connectMsg = "Connect Database";

    private final OrderRepository orderRepository;
    private final OrderMapper mapper;
    private final StatusEnumStatusConverter statusConverter;
    @Autowired
    private OrderService service; // Autowired for calling cache from another method of the same bean

    @CacheEvict(allEntries = true)
    public OrderSummary save(List<OrderDto> dto) {
        log.info("save");
        log.info(connectMsg);
        return mapper.get(orderRepository.saveAll(mapper.save(dto)));
    }

    public List<OrderSummary> findAll(StatusEnum.CommonStatus status) {
        log.info("findAll");
        return service.dtoListFindAll(status);
    }

    public List<OrderDto> findById(UUID id) {
        log.info("findOrderDetailByOrderId");
        return service.orderById(id);
    }

    public List<OrderSummary> findByAccount(UUID account, StatusEnum.CommonStatus status) {
        log.info("findByAccount");
        return service.orderByAccountId(account, status, Boolean.FALSE);
    }

    public List<OrderDto> patchStatus(UUID id, StatusEnum.CommonStatus status) {
        log.info("partialStatus");
        return service.updatedStatus(id, status);
    }

    public List<OrderDto> deleted(UUID id) {
        log.info("delete");
        return service.deletedOrder(id);
    }

    @Cacheable(unless = "#result.isEmpty()")
    public List<OrderSummary> dtoListFindAll(StatusEnum.CommonStatus status) {
        log.info(connectMsg);
        return (status == null) ? orderRepository.getSummaryAllIsDeleted(Boolean.FALSE) : orderRepository.getSummaryAllByStatusAndIsDeleted(statusConverter.convertToDatabaseColumn(status), Boolean.FALSE);
    }

    @Cacheable(cacheNames = ORDER, unless = "#result==null")
    public List<OrderDto> orderById(UUID id) {
        log.info(connectMsg);
        return mapper.map(orderRepository.getAllById(id));
    }

    @Cacheable(unless = "#result==null")
    public List<OrderSummary> orderByAccountId(UUID id, StatusEnum.CommonStatus status, Boolean isDeleted) {
        log.info(connectMsg);
        return (status == null) ? orderRepository.getAllByAccountAndIsDeleted(id, isDeleted) : orderRepository.getAllByAccountAndStatusAndIsDeleted(id, statusConverter.convertToDatabaseColumn(status), isDeleted);
    }

    @Transactional
    @Caching(evict = {@CacheEvict(allEntries = true)}, put = @CachePut(cacheNames = ORDER, key = "#id"))
    public List<OrderDto> updatedStatus(UUID id, StatusEnum.CommonStatus status) {
        log.info(connectMsg);
        orderRepository.updateScore(id, statusConverter.convertToDatabaseColumn(status));
        return service.orderById(id);
    }

    @Transactional
    @Caching(evict = {@CacheEvict(allEntries = true), @CacheEvict(cacheNames = ORDERS_RECYCLEBIN, allEntries = true)}, put = @CachePut(cacheNames = ORDER, key = "#id"))
    public List<OrderDto> deletedOrder(UUID id) {
        log.info(connectMsg);
        orderRepository.deleteById(id);
        return service.orderById(id);
    }
}
