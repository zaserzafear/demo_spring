package com.zaserzafear.demo_spring.service;

import com.zaserzafear.demo_spring.entity.ProductTransaction;
import com.zaserzafear.demo_spring.entity.converter.InOutEnum;
import com.zaserzafear.demo_spring.repository.ProductTransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.zaserzafear.demo_spring.config.CacheConf.CacheName.PRODUCT_TRANSACTIONS;


@Log4j2
@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = PRODUCT_TRANSACTIONS)
public class ProductTransactionService {

    private final ProductTransactionRepository productTransactionRepository;

    @Cacheable(unless = "#result.isEmpty()")
    public List<Object[]> findAll() {
        log.info("findAll");
        return productTransactionRepository.getProductTransactionAll();
    }

    @CacheEvict(allEntries = true)
    public ProductTransaction saveProductTransaction(String productCode, InOutEnum.Action action, Integer productQuantity, Double productPrice) {
        log.info("saveProductTransaction");
        return productTransactionRepository.save(new ProductTransaction(productCode, action, productQuantity, productPrice));
    }

    @CacheEvict(allEntries = true)
    public List<ProductTransaction> saveAllProductTransaction(List<ProductTransaction> productTransactionList) {
        log.info("saveAllProductTransaction");
        return productTransactionRepository.saveAll(productTransactionList);
    }
}
