package com.zaserzafear.demo_spring.service;

import com.zaserzafear.demo_spring.entity.ProductCategory;
import com.zaserzafear.demo_spring.exception.NotFoundException;
import com.zaserzafear.demo_spring.repository.ProductCategoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.zaserzafear.demo_spring.config.CacheConf.CacheName.PRODUCT_CATEGORY;

@Log4j2
@RequiredArgsConstructor
@Service
@CacheConfig(cacheNames = PRODUCT_CATEGORY)
public class ProductCategoryService {

    private final ProductCategoryRepository productCategoryRepository;

    @Cacheable(cacheNames = PRODUCT_CATEGORY)
    public ProductCategory findCategory(UUID id) {
        log.info("findCategory : " + id);
        return productCategoryRepository.findById(id).orElseThrow(() -> new NotFoundException("Product Category Id : " + id + " Not Found In Database"));
    }
}
