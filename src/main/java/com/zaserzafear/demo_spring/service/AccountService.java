package com.zaserzafear.demo_spring.service;

import com.zaserzafear.demo_spring.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@RequiredArgsConstructor
@Service
public class AccountService {

    private static final String connectMsg = "Connect Database";

    private final AccountRepository accountRepository;
}
