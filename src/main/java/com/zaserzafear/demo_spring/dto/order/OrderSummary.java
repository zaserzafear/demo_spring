package com.zaserzafear.demo_spring.dto.order;


import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

public interface OrderSummary {
    @Value("#{@stringToUuidConverter.uuid(target.id)}")
    UUID getId();

    Integer getProduct();

    @Value("#{@stringToUuidConverter.uuid(target.account)}")
    UUID getAccount();

    Integer getQuantity();

    Double getPrice();

    String getOrderDate();

    String getUpdatedDate();

    @Value("#{@statusEnumStatusConverter.convertToEntityAttribute(target.status)}")
    StatusEnum.CommonStatus getStatus();
}
