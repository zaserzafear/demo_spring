package com.zaserzafear.demo_spring.dto.order;

import com.zaserzafear.demo_spring.dto.common.CommonStatusDto;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class OrderDto extends CommonStatusDto {

    private UUID account;
    private UUID product;
    private Integer quantity;
    private Double price;
    private OffsetDateTime orderDate;

    public OrderDto(UUID id, UUID product, UUID account, Integer quantity, Double price, OffsetDateTime orderDate, StatusEnum.CommonStatus status, OffsetDateTime updatedDate, Boolean isDeleted) {
        this.id = id;
        this.product = product;
        this.account = account;
        this.quantity = quantity;
        this.price = price;
        this.orderDate = orderDate;
        this.status = status;
        this.updatedDate = updatedDate;
        this.isDelete = isDeleted;
    }
}
