package com.zaserzafear.demo_spring.dto.order;

import com.zaserzafear.demo_spring.entity.Order;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import com.zaserzafear.demo_spring.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.zaserzafear.demo_spring.entity.converter.StatusEnum.CommonStatus.PENDING;

@Log4j2
@RequiredArgsConstructor
@Component
public class OrderSetMapper implements OrderMapper {

    private final OrderRepository repo;

    @Override
    public List<Order> save(List<OrderDto> dto) {
        log.info("List<Order> save(List<OrderDto> dto)");
        if (dto == null) return null;
        UUID id;
        if (StringUtils.isEmpty(dto.get(0).getId())) {
            id = UUID.randomUUID();
        } else {
            id = dto.get(0).getId();
        }
        UUID account = dto.get(0).getAccount();
        OffsetDateTime orderData;
        if (dto.get(0).getOrderDate() != null) {
            orderData = dto.get(0).getOrderDate();
        } else {
            orderData = OffsetDateTime.now();
        }
        StatusEnum.CommonStatus status;
        if (dto.get(0).status != null) {
            status = dto.get(0).getStatus();
        } else {
            status = PENDING;
        }

        List<Order> entity = new ArrayList<>();
        if (!StringUtils.isEmpty(dto.get(0).getId())) entity = repo.getAllById(dto.get(0).getId());
        List<Order> finalEntity = entity;
        dto.stream().parallel().forEachOrdered(
                (e) -> finalEntity.add(new Order(id, e.getProduct(), orderData, account, e.getQuantity(), e.getPrice(), status))
        );
        return finalEntity;
    }

    @Override
    public OrderSummary get(List<Order> entity) {
        log.info("OrderSummary get(List<Order> entity)");
        if (entity == null) return null;
        UUID id = entity.get(0).getId();
        return repo.getSummaryById(id);
    }

    @Override
    public List<OrderDto> map(List<Order> entity) {
        log.info("List<OrderDto> map(List<Order> entity)");
        if (entity == null) return null;
        List<OrderDto> dto = new ArrayList<>();
        entity.stream().parallel().forEachOrdered(
                (e) -> dto.add(new OrderDto(e.getId(), e.getProduct(), e.getAccount(), e.getQuantity(), e.getPrice(), e.getOrderDate(), e.getStatus(), e.getUpdatedDate(), e.getIsDeleted()))
        );
        return dto;
    }
}
