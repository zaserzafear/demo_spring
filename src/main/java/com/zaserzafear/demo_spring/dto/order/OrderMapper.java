package com.zaserzafear.demo_spring.dto.order;

import com.zaserzafear.demo_spring.entity.Order;

import java.util.List;

public interface OrderMapper {

    List<Order> save(List<OrderDto> dto);

    OrderSummary get(List<Order> entity);

    List<OrderDto> map(List<Order> entity);
}
