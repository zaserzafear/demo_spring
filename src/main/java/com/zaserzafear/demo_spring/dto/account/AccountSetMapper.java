package com.zaserzafear.demo_spring.dto.account;


import com.zaserzafear.demo_spring.entity.Account;
import com.zaserzafear.demo_spring.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class AccountSetMapper implements AccountMapper {

    private final AccountRepository repo;
    private OffsetDateTime deleteDate;

    @Override
    public Account map(AccountDto dto) {
        if (dto == null) return null;
        Account entity = new Account();
        if (!StringUtils.isEmpty(dto.getId())) entity = repo.findById(dto.getId()).get();
        String[] name = dto.getFullName().split(" ");
        entity.setFirst_name(name[0]);
        entity.setLast_name(name[1]);
        entity.setCoin(dto.getCoin());
        entity.setIsDeleted(dto.getIsDelete());
        entity.setDeletedDate(deleteDate);
        deleteDate = null;
        return entity;
    }

    @Override
    public AccountDto map(Account entity) {
        if (entity == null) return null;
        AccountDto dto = new AccountDto();
        dto.setId(entity.getId());
        dto.setFullName(entity.getFirst_name() + " " + entity.getLast_name());
        dto.setCoin(entity.getCoin());
        dto.setUpdatedDate((entity.getUpdatedDate() != null) ? entity.getUpdatedDate() : entity.getCreatedDate());
        dto.setIsDelete(entity.getIsDeleted());
        deleteDate = entity.getDeletedDate();
        return dto;
    }

    @Override
    public List<AccountDto> map(List<Account> entity) {
        if (entity == null) return null;
        List<AccountDto> dto = new ArrayList<>();
        entity.stream().parallel().forEachOrdered(
                (e) -> dto.add(new AccountDto(e.getId(), e.getFirst_name() + e.getLast_name(), e.getCoin(), (e.getUpdatedDate() != null) ? e.getUpdatedDate() : e.getCreatedDate()))
        );
        return dto;
    }
}
