package com.zaserzafear.demo_spring.dto.account;


import com.zaserzafear.demo_spring.entity.Account;

import java.util.List;

public interface AccountMapper {

    Account map(AccountDto dto);

    AccountDto map(Account entity);

    List<AccountDto> map(List<Account> entity);
}
