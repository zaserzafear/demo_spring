package com.zaserzafear.demo_spring.dto.account;

import com.zaserzafear.demo_spring.dto.common.CommonStatusDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class AccountDto extends CommonStatusDto {

    private String fullName;
    private Double coin;

    // custom constructor for mapper to list
    public AccountDto(UUID id, String fullName, Double coin, OffsetDateTime updatedDate) {
        this.id = id;
        this.fullName = fullName;
        this.coin = coin;
        this.updatedDate = updatedDate;
    }
}
