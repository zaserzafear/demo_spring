package com.zaserzafear.demo_spring.dto.common;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@Setter
public class CommonDto {

    public UUID id;
    public OffsetDateTime updatedDate;
    public Boolean isDelete;
}
