package com.zaserzafear.demo_spring.dto.common;

import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommonStatusDto extends CommonDto {
    public StatusEnum.CommonStatus status;
}
