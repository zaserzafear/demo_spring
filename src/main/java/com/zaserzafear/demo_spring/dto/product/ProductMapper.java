package com.zaserzafear.demo_spring.dto.product;

import com.zaserzafear.demo_spring.entity.Product;

import java.util.List;

public interface ProductMapper {

    ProductDto map(Product entity);

    Product map(ProductDto dto);

    List<ProductDto> map(List<Product> entity);
}
