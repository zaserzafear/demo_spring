package com.zaserzafear.demo_spring.dto.product;

import com.zaserzafear.demo_spring.dto.common.CommonStatusDto;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class ProductDto extends CommonStatusDto {

    private UUID category;
    private String code;
    private String name;
    private Integer quantity;
    private Double price;

    // custom constructor for mapper to list
    public ProductDto(UUID id, UUID category, String code, String name, Integer quantity, Double price, StatusEnum.CommonStatus status, OffsetDateTime updatedDate) {
        this.id = id;
        this.category = category;
        this.code = code;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
        this.updatedDate = updatedDate;
    }
}
