package com.zaserzafear.demo_spring.dto.product;

import com.zaserzafear.demo_spring.entity.Product;
import com.zaserzafear.demo_spring.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.zaserzafear.demo_spring.entity.converter.StatusEnum.CommonStatus.PENDING;

@Log4j2
@RequiredArgsConstructor
@Component
public class ProductSetMapper implements ProductMapper {

    private final ProductRepository repo;
    private OffsetDateTime deleteDate;

    @Override
    public Product map(ProductDto dto) {
        if (dto == null) return null;
        Product entity = new Product();
        if (!StringUtils.isEmpty(dto.getId())) entity = repo.findById(dto.getId()).get();
        entity.setId(dto.getId());
        entity.setCategory(dto.getCategory());
        entity.setCode(dto.getCode());
        entity.setName(dto.getName());
        entity.setQuantity(dto.getQuantity());
        entity.setPrice(dto.getPrice());
        entity.setStatus((dto.getStatus() != null) ? dto.getStatus() : PENDING);
        entity.setIsDeleted((dto.getIsDelete() != null) ? dto.getIsDelete() : Boolean.FALSE);
        entity.setDeletedDate(deleteDate);
        deleteDate = null;
        return entity;
    }

    @Override
    public ProductDto map(Product entity) {
        if (entity == null) return null;
        ProductDto dto = new ProductDto();
        dto.setId(entity.getId());
        dto.setCategory(entity.getCategory());
        dto.setCode(entity.getCode());
        dto.setName(entity.getName());
        dto.setQuantity(entity.getQuantity());
        dto.setPrice(entity.getPrice());
        dto.setStatus(entity.getStatus());
        dto.setUpdatedDate((entity.getUpdatedDate() != null) ? entity.getUpdatedDate() : entity.getCreatedDate());
        dto.setIsDelete(entity.getIsDeleted());
        deleteDate = entity.getDeletedDate();
        return dto;
    }

    @Override
    public List<ProductDto> map(List<Product> entity) {
        List<ProductDto> dto = new ArrayList<>(entity.size());
        entity.stream().parallel().forEachOrdered(
                (e) -> dto.add(new ProductDto(e.getId(), e.getCategory(), e.getCode(), e.getName(), e.getQuantity(), e.getPrice(), e.getStatus(), (e.getUpdatedDate() != null) ? e.getUpdatedDate() : e.getCreatedDate()))
        );
        return dto;
    }
}
