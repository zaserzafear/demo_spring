package com.zaserzafear.demo_spring.repository;

import com.zaserzafear.demo_spring.entity.ProductCategory;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductCategoryRepository extends CommonRepository<ProductCategory, UUID> {
}
