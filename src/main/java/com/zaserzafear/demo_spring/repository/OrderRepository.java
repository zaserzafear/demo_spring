package com.zaserzafear.demo_spring.repository;

import com.zaserzafear.demo_spring.dto.order.OrderSummary;
import com.zaserzafear.demo_spring.entity.Order;
import com.zaserzafear.demo_spring.entity.OrderId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends CommonRepository<Order, OrderId> {

//    String getOrderAll = "SELECT cast(o.o_id as varchar) AS id" +
//            ", a.first_name || ' ' || a.last_name AS accountName" +
//            ", SUM(o.quantity) AS productQuantity" +
//            ", SELECT SUM(o2.quantity * o2.price) FROM orders o2 WHERE o2.o_id = o.o_id  AS productPriceTotal" +
//            ", o.order_date AS date" +
//            ", o.status AS status" +
//            " FROM orders o" +
//            " JOIN products p" +
//            " ON o.p_code = p.code" +
//            " JOIN accounts a" +
//            " ON o.account = a.id" +
//            " ";
//    String getGetOrderAllGroupBy = " GROUP BY o.o_id";
//    String getOrderDetail = "SELECT cast(o.o_id as varchar) AS id" +
//            ", a.first_name || ' ' || a.last_name AS accountName" +
//            ", p.code AS productCode" +
//            ", p.name AS productName" +
//            ", o.quantity AS productQuantity" +
//            ", o.price AS productPrice" +
//            ", o.quantity * o.price AS productPriceTotal" +
//            ", o.order_date AS date" +
//            ", o.status AS status" +
//            " FROM orders o" +
//            " JOIN products p" +
//            " ON o.p_code = p.code" +
//            " JOIN accounts a" +
//            " ON o.account = a.id" +
//            " ";
//    String allField = "o_id, p_code, account, quantity, price , order_date, status, created_date, updated_date, is_deleted, deleted_date, version";
//
//    @Query(value = getOrderAll + "WHERE o.is_delete = false" + getGetOrderAllGroupBy, nativeQuery = true)
//    List<Object[]> getOrderAll();
//
//    @Query(value = getOrderAll + "WHERE o.is_delete = false AND o.status = :status" + getGetOrderAllGroupBy, nativeQuery = true)
//    List<Object[]> getOrderByOrderStatus(@Param("status") String orderStatus);
//
//    @Query(value = getOrderAll + "WHERE o.is_delete = false AND o.account = :a_id" + getGetOrderAllGroupBy, nativeQuery = true)
//    List<Object[]> getOrderAllByAccountId(@Param("a_id") UUID accountId);
//
//    @Query(value = getOrderAll + "WHERE o.is_delete = true" + getGetOrderAllGroupBy, nativeQuery = true)
//    List<Object[]> getOrderRecycleBin();
//
//    @Query(value = getOrderAll + "WHERE o.is_delete = true AND o.status = :status" + getGetOrderAllGroupBy, nativeQuery = true)
//    List<Object[]> getOrderRecycleBinByOrderStatus(@Param("status") String status);
//
//    @Query(value = getOrderDetail + "WHERE o.o_id = :o_id", nativeQuery = true)
//    List<Object[]> getOrderDetailByOrderId(@Param("o_id") UUID orderId);

//    @Query(value = getOrderDetail + " WHERE o.o_id = :o_id AND a.id = :a_id", nativeQuery = true)
//    List<OrderDetailDto> getOrderDetailByOrderIdAndAccountId(@Param("o_id") UUID orderId, @Param("a_id") UUID accountId);
//
//    @Query(value = getOrderAll + "WHERE o.is_delete = true AND o.account = :a_id" + getGetOrderAllGroupBy, nativeQuery = true)
//    List<Object[]> getOrderRecycleBinByAccountId(@Param("a_id") UUID accountId);

    String selectFindAll = "SELECT DISTINCT CAST(o.o_id AS varchar) AS id" +
            ", (SELECT COUNT(o2.p_id) FROM orders o2 WHERE o.o_id = o2.o_id) AS product" +
            ", CAST(o.account AS varchar) AS account" +
            ", (SELECT SUM(o3.quantity) FROM orders o3 WHERE o.o_id = o3.o_id) AS quantity" +
            ", (SELECT SUM(o4.quantity * o4.price) FROM orders o4 WHERE o.o_id = o4.o_id) AS price" +
            ", o.order_date AS orderDate" +
            ", CASE WHEN o.updated_date IS NULL" +
            "       THEN MAX(o.created_date)" +
            "       ELSE MAX(o.updated_date)" +
            "  END AS updatedDate" +
            ", o.status AS status" +
            ", o.is_deleted AS isDeleted" +
            ", o.version AS version" +
            " FROM orders o";
    String selectFindAllGroupBy = " GROUP BY o.o_id";

    @Query(value = "SELECT * FROM orders WHERE o_id = :o_id", nativeQuery = true)
    List<Order> getAllById(@Param("o_id") UUID o_id);

    @Query(value = selectFindAll + " WHERE o.o_id = :o_id" + selectFindAllGroupBy, nativeQuery = true)
    OrderSummary getSummaryById(@Param("o_id") UUID o_id);

    @Query(value = selectFindAll + " WHERE o.is_deleted = :isDeleted" + selectFindAllGroupBy, nativeQuery = true)
    List<OrderSummary> getSummaryAllIsDeleted(@Param("isDeleted") Boolean isDeleted);

    @Query(value = selectFindAll + " WHERE o.status = :status AND o.is_deleted = :isDeleted" + selectFindAllGroupBy, nativeQuery = true)
    List<OrderSummary> getSummaryAllByStatusAndIsDeleted(@Param("status") String status, @Param("isDeleted") Boolean isDeleted);

    @Query(value = selectFindAll + " WHERE o.account = :account AND o.is_deleted = :isDeleted" + selectFindAllGroupBy, nativeQuery = true)
    List<OrderSummary> getAllByAccountAndIsDeleted(@Param("account") UUID account, @Param("isDeleted") Boolean isDeleted);

    @Query(value = selectFindAll + " WHERE o.account = :account AND o.status = :status AND o.is_deleted = :isDeleted" + selectFindAllGroupBy, nativeQuery = true)
    List<OrderSummary> getAllByAccountAndStatusAndIsDeleted(@Param("account") UUID account, @Param("status") String status, @Param("isDeleted") Boolean isDeleted);

    @Modifying
    @Query(value = "UPDATE orders SET updated_date = NOW(), version = version + 1, status = :status WHERE o_id = :o_id", nativeQuery = true)
    Integer updateScore(@Param("o_id") UUID o_id, @Param("status") String status);

    @Modifying
    @Query(value = "UPDATE orders SET updated_date = NOW(), version = version + 1, is_deleted = TRUE, deleted_date = NOW() WHERE o_id = :o_id", nativeQuery = true)
    Integer deleteById(@Param("o_id") UUID o_id);
}
