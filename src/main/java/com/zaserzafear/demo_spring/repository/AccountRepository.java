package com.zaserzafear.demo_spring.repository;

import com.zaserzafear.demo_spring.entity.Account;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AccountRepository extends CommonRepository<Account, UUID> {
}
