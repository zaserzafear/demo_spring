package com.zaserzafear.demo_spring.repository;

import com.zaserzafear.demo_spring.entity.Product;
import com.zaserzafear.demo_spring.entity.converter.StatusEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductRepository extends CommonRepository<Product, UUID> {

//    String getProductAll = "SELECT cast(p.id AS VARCHAR) AS id" +
//            ", cast(c.id AS VARCHAR) AS productCategoryId" +
//            ", c.name AS productCategoryName" +
//            ", p.code AS productCode" +
//            ", p.name AS productName" +
//            ", p.quantity AS productQuantity" +
//            ", p.price AS productPrice" +
//            ", p.status AS status" +
//            " FROM products p JOIN product_categories c" +
//            " ON p.category = c.id" +
//            " ";
//
//    @Query(value = getProductAll + "WHERE p.is_delete = false", nativeQuery = true)
//    List<Object[]> getProductAll();
//
//    @Query(value = getProductAll + "WHERE p.is_delete = false AND p.status = :status", nativeQuery = true)
//    List<Object[]> getProductByProductStatus(@Param("status") String productStatus);
//
//    @Query(value = getProductAll + "WHERE p.is_delete = false AND c.code = :c_code", nativeQuery = true)
//    List<Object[]> getProductByProductCategoryCode(@Param("c_code") String productCategoryCode);
//
//    @Query(value = getProductAll + "WHERE p.is_delete = true ORDER BY p.delete_date DESC", nativeQuery = true)
//    List<Object[]> getProductRecycleBin();
//
//    @Query(value = getProductAll + "WHERE p.is_delete = true AND p.status = :status", nativeQuery = true)
//    List<Object[]> getProductRecycleBinByProductStatus(@Param("status") String productStatus);
//
//    @Query(value = getProductAll + "WHERE p.is_delete = true AND c.code = :c_code ORDER BY p.delete_date DESC", nativeQuery = true)
//    List<Object[]> getProductRecycleBinByProductCategoryCode(@Param("c_code") String productCategoryCode);
//
//    @Query(value = getProductAll + "WHERE p.code = :p_code", nativeQuery = true)
//    Optional<Object[]> getProductByProductCode(@Param("p_code") String productCode);

    List<Product> findAllByIsDeleted(Boolean isDelete);

    List<Product> findAllByStatusAndIsDeleted(StatusEnum.CommonStatus status, Boolean isDelete);

    List<Product> findAllByCategoryAndIsDeleted(UUID category, Boolean isDelete);

    @Query(value = "SELECT * FROM products WHERE id=?1", nativeQuery = true)
    Product findId(UUID id);

    Product findByCode(String code);
}
