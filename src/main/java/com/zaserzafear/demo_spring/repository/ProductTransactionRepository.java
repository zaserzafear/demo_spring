package com.zaserzafear.demo_spring.repository;

import com.zaserzafear.demo_spring.entity.ProductTransaction;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductTransactionRepository extends CommonRepository<ProductTransaction, UUID> {
}
